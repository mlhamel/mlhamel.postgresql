Readme
======

Deployment and configuration for mlhamel postgresql and it's network, it is quite simple
to use, just run the following commands:

    $ mkvirtualenv mlhamel.postgresql
    $ pip install -r requirements.txt
    $ make
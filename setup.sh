#!/bin/bash
echo "******CREATING DOCKER DATABASE******"
gosu postgres postgres --single <<- EOSQL
   CREATE USER root;
   GRANT ALL PRIVILEGES;
EOSQL
echo ""
echo "******DOCKER DATABASE CREATED******

ifndef message
	message = "Updating postgresql app"
endif

ifndef repos
	repos = mlhamel/postgresql
endif

ifndef name
	name = postgresql
endif

.SHELLFLAGS = -e
.PHONY: docker-build
.NOTPARALLEL:

default: build
build: docker-build
commit: docker-commit
push: docker-push
tag: docker-tag
docker-build: do-docker-build
docker-commit: do-docker-commit
docker-push: do-docker-push
docker-tag: do-docker-tag

do-docker-build:
	docker build -t $(name) --no-cache --rm . | tee build.log || exit 1

do-docker-commit:
	docker commit -m $(message) $(revision) $(repos)

do-docker-push:
	docker push $(repos)

do-docker-tag:
	docker tag -f $(name):$(tag) $(repos):$(tag)

# Version Bump using bumpversion
patch:
	bumpversion patch
major:
	bumpversion major
minor:
	bumpversion minor

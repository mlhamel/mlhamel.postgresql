import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, "README.md")).read()

install_requires = [
    "bumpversion",
]

setup(
    name="mlhamel.postgresql",
    version="0.1.0",
    description="mlhamel.postgresql container",
    long_description=README + "\n\n",
    author="Mathieu Leduc-Hamel",
    author_email="mlhamel.org",
    url="http://mlhamel.org",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires)
